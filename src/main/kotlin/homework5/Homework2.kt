package homework5

fun main() {
    val digit = readLine()!!.toInt()
    println(reverseNumWithoutZeros(digit))
    reverseNumWithZeros(digit)
}

fun reverseNumWithoutZeros(digit : Int) : Int {
    //выводит перевернутое число без незначащих нулей
    var res = 0
    var temp = digit
    while (temp > 0){
        res = res * 10 + temp % 10
        temp /= 10
    }
    return res
}

fun reverseNumWithZeros(digit : Int) {
    //выводит перевернутое число как строку - с незначащими нулями
    var temp = digit
    while (temp > 0){
        print(temp % 10)
        temp /= 10
    }
}
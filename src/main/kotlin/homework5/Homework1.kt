package homework5

fun main(){
    val foodPreferences = arrayOf("Antelope", "Zebra", "Buffalo", "Meat", "Deer", "Roe", "Boar",
        "Grass", "Vegetables", "Leafs", "Rye", "Banana", "Apple", "Rat", "Bamboo", "Nuts", "Celery",
        "Fruits")
    val meal = foodPreferences[(0 until foodPreferences.size - 1).random()]
    val myLion = Lion("Alex", 1.2, 73.5)
    val myTiger = Tiger("Tim", 1.3, 102.5)
    val myHippo = Hippo("Harry", 2.2, 1370.5)
    val myWolf = Wolf("Wick", 0.7, 33.5)
    val myGiraffe = Giraffe("Greg", 3.7, 183.5)
    val myElephant = Elephant("Elliot", 3.4, 233.5)
    val myChimpanzee = Chimpanzee("Charles", 1.1, 33.5)
    val myGorilla = Gorilla("George", 1.8, 103.5)
    println("Lion ${myLion.name}, tiger ${myTiger.name}, hippo ${myHippo.name}, wolf ${myWolf.name}, " +
            "giraffe ${myGiraffe.name}, elephant ${myElephant.name}, chimpanzee ${myChimpanzee.name} " +
            "and gorilla ${myGorilla.name} had saturation = ${myLion.saturation},")
    println("then they ate $meal")
    myLion.eat(meal)
    myTiger.eat(meal)
    myHippo.eat(meal)
    myWolf.eat(meal)
    myGiraffe.eat(meal)
    myElephant.eat(meal)
    myChimpanzee.eat(meal)
    myGorilla.eat(meal)
    println("Now they have saturation: lion - ${myLion.saturation}, tiger - ${myTiger.saturation}, " +
            "hippo - ${myHippo.saturation}, wolf - ${myWolf.saturation}, giraffe - ${myGiraffe.saturation}, " +
            "elephant - ${myElephant.saturation}, chimpanzee - ${myChimpanzee.saturation} " +
            "and gorilla - ${myGorilla.saturation}")
}
open class Animal(var name: String, var height: Double, var weight: Double){
    open val foodPreferences = emptyArray<String>()
    var saturation = 0
    fun eat(meal: String){
        for (element in foodPreferences){
            if (element == meal){
                saturation++
                break
            }
        }
    }
}

class Lion(name: String, height: Double, weight: Double): Animal(name, height, weight){
    override val foodPreferences = arrayOf("Antelope", "Zebra", "Buffalo", "Meat")
}

class Tiger(name: String, height: Double, weight: Double): Animal(name, height, weight){
    override val foodPreferences = arrayOf("Deer", "Roe", "Boar", "Meat")
}

class Hippo(name: String, height: Double, weight: Double): Animal(name, height, weight){
    override val foodPreferences = arrayOf("Grass", "Vegetables")
}

class Wolf(name: String, height: Double, weight: Double): Animal(name, height, weight){
    override val foodPreferences = arrayOf("Deer", "Roe", "Boar", "Meat")
}

class Giraffe(name: String, height: Double, weight: Double): Animal(name, height, weight){
    override val foodPreferences = arrayOf("Leafs", "Grass")
}

class Elephant(name: String, height: Double, weight: Double): Animal(name, height, weight){
    override val foodPreferences = arrayOf("Grass", "Rye", "Banana", "Apple")
}

class Chimpanzee(name: String, height: Double, weight: Double): Animal(name, height, weight){
    override val foodPreferences = arrayOf("Banana", "Fruits", "Rat")
}

class Gorilla(name: String, height: Double, weight: Double): Animal(name, height, weight){
    override val foodPreferences = arrayOf("Bamboo", "Nuts", "Celery")
}
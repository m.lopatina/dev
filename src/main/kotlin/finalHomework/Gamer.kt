package finalHomework

class Gamer (val name: String) {                                             //класс Игрок
    var board: Array<Array<Int>> = Array(4) { Array(4) { 0 } }
    private val listOfShips = listOf(                                       //лист с объектами класса корабль
        createShip(board, 2),
        createShip(board, 1),
        createShip(board, 1))
    var loosingList = mutableMapOf(                                         //список координат кораблей и их состояния
        listOfShips[0].coordinates to "alive",
        listOfShips[1].coordinates to "alive",
        listOfShips[2].coordinates to "alive"
    )
    fun boardPrint() {                                                      //для печати поля
        println("This is your battlefield. Good luck, $name!")
        for (row in board) {
            print("|")
            for (cell in row) {
                when (cell) {
                    0 -> print("~|")
                    1 -> print("~|")
                    2 -> print("S|")
                }
            }
            println()
        }
    }
}
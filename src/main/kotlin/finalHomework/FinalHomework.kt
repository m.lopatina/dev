package finalHomework
fun main() {
    seaBattleGame()
}
fun createShip(board: Array<Array<Int>>, levels: Int): Ship {               //создание корабля
    var shipCords: Int
    println("Enter coordinates")
    do {
        try{
            shipCords = readLine()!!.toInt() - 11
        } catch (e: NumberFormatException){
            println("Wrong coordinates. Try again")
            continue
        }
        val row = shipCords / 10
        val col = shipCords % 10
        if (col in 0..3 && row in 0..3 && board[row][col] == 0) {
            board[row][col] = 2
            surrounding(board, row, col)
            if (levels > 1){
                shipCords = (shipCords + 11) * 100 + additionalLevel(board, row, col)
            }
            break
        } else {
            println("Wrong coordinates. Try again")
            continue
        }
    }
    while (true)
    return Ship(shipCords + 11)
}
fun additionalLevel(board: Array<Array<Int>>, row: Int, col: Int): Int {    //создание дополнительной палубы
    var shipCords: Int
    println("Enter coordinates")
    do {
        try{
            shipCords = readLine()!!.toInt() - 11
        } catch (e: NumberFormatException){
            println("Wrong coordinates. Try again")
            continue
        }
        if ((kotlin.math.abs(row - shipCords / 10) == 1 && col == shipCords % 10 ||
                    kotlin.math.abs(col - shipCords % 10) == 1 && row == shipCords / 10) &&
            (board[shipCords / 10][shipCords % 10] != 2)) {
            board[shipCords / 10][shipCords % 10] = 2
            surrounding(board, shipCords / 10, shipCords % 10)
            break
        } else {
            println("Wrong coordinates. Try again")
            continue
        }
    } while(true)
    return shipCords
}
fun surrounding(board: Array<Array<Int>>, row: Int, col: Int) {         //окружение корабля зоной, куда нельзя поставить еще один корабль

    for (i in row - 1..row + 1) {
        for (j in col - 1..col + 1) {
            try {
                if (board[i][j] != 2){
                    board[i][j] = 1
                }
            } catch (e: ArrayIndexOutOfBoundsException) {
                continue
            }
        }
    }
}
fun seaBattleGame(){
    var win = false
    println("Hi! This is a sea battle game.\n" +
            "Playing field has coordinates from 1 to 4.\n" +
            "Introduce yourself and place first one two-deck and two single-deck ships on the field at a distance of at least one cell from each other.\n" +
            "Enter the coordinates in turn in the form of two consecutive digits - row and column number.\n" +
            "For example, 11 is the first row and first column cell")
    println("Gamer #1, Introduce yourself")
    val gamer1 = Gamer(readLine().toString())
    gamer1.boardPrint()
    println("Gamer #1, Introduce yourself")
    val gamer2 = Gamer(readLine().toString())
    gamer2.boardPrint()
    var mover = gamer1
    var waiter = gamer2
    var buf: Any
    var move: Int
    do{
        println("${mover.name}, it's your turn. Enter coordinates")
        try{
            move = readLine()!!.toInt()
        } catch (e: NumberFormatException){
            println("Wrong coordinates. Try again")
            continue
        }
        if (((move / 10) in 1..4) && ((move % 10) in 1..4)) {
           if (waiter.loosingList.containsKey(move)) {                      //проверка, что это однопалубный
               if (waiter.loosingList[move] != "dead"){
               println("Drowned!")
               waiter.loosingList[move] = "dead"
                   win = !(waiter.loosingList.containsValue("alive") || waiter.loosingList.containsValue("hit"))    //проверка на победу
               } else {
                   println("Already dead!")
                   buf = mover
                   mover = waiter
                   waiter = buf
                   continue
               }
           } else{
               val keyC = waiter.loosingList.keys.toList()
               if (keyC[0] / 100 == move || keyC[0] % 100 == move){         //проверка, может, двухпалубный
                       when (waiter.loosingList[keyC[0]]) {
                           "dead" -> {
                               println("Already dead!")
                               buf = mover
                               mover = waiter
                               waiter = buf
                               continue
                           }
                           "alive" -> {
                               println("Hit!")
                               waiter.loosingList[keyC[0]] = "hit"
                               continue
                           }
                           "hit" -> {
                               println("Drowned!")
                               waiter.loosingList[keyC[0]] = "dead"
                           }
                       }
                   } else {
                   println("Missed!")
                   buf = mover
                   mover = waiter
                   waiter = buf
                   continue
               }
           }
        } else {
            println("Wrong coordinates. Try again")
            continue
        }
    }
        while(!win)
        println("AAAAAaaaaaand the winner is ${mover.name}! Congratulations!")
}
package homework7

import java.lang.Exception

fun main() {
    val newUser = createNewMember(readLine().toString(), readLine().toString(), readLine().toString())
    println("Welcome, ${newUser.login}! Your profile is ready!")
}
fun createNewMember(log: String, pass: String, confPass: String): User {
    if (log.length > 20) {
        throw WrongLoginException("Login must be 20 characters or less")
    }
    if (pass.length < 10) {
        throw WrongPasswordException("Password must be 10 characters or more")
    }
    if (pass != confPass) {
        throw WrongPasswordException("Passwords doesn't match")
    } else {
        return User(log, pass)
    }
}
class WrongLoginException(message: String): Exception(message)
class WrongPasswordException(message: String): Exception(message)
class User(val login: String, val password: String)

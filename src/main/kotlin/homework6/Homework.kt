package homework6
fun main(){
    val foodPreferences = arrayOf("Antelope", "Zebra", "Buffalo", "Meat", "Deer", "Roe", "Boar",
        "Grass", "Vegetables", "Leafs", "Rye", "Banana", "Apple", "Rat", "Bamboo", "Nuts", "Celery",
        "Fruits")
    val meal = Array((2..5).random()) { foodPreferences[(0 until foodPreferences.size - 1).random()] }
    val zoo = arrayOf(
        Lion("Lion", 1.2, 73.5),
        Tiger("Tiger", 1.3, 102.5),
        Hippo("Hippo", 2.2, 1370.5),
        Wolf("Wolf", 0.7, 33.5),
        Giraffe("Giraffe", 3.7, 183.5),
        Elephant("Elephant", 3.4, 233.5),
        Chimpanzee("Chimpanzee", 1.1, 33.5),
        Gorilla("Gorilla", 1.8, 103.5)
    )
    feeding(zoo, meal)
}
abstract class Animal(open val name: String, open val height: Double, open val weight: Double){
    abstract val foodPreferences: Array<String>
    var saturation = 0
    fun eat(meal: String){
        if (meal in foodPreferences){
                saturation++
        }
    }
}
class Lion(override val name: String, override val height: Double, override val weight: Double): Animal(name, height, weight){
    override val foodPreferences = arrayOf("Antelope", "Zebra", "Buffalo", "Meat")
}
class Tiger(override val name: String, override val height: Double, override val weight: Double): Animal(name, height, weight){
    override val foodPreferences = arrayOf("Deer", "Roe", "Boar", "Meat")
}
class Hippo(override val name: String, override val height: Double, override val weight: Double): Animal(name, height, weight){
    override val foodPreferences = arrayOf("Grass", "Vegetables")
}
class Wolf(override val name: String, override val height: Double, override val weight: Double): Animal(name, height, weight){
    override val foodPreferences = arrayOf("Deer", "Roe", "Boar", "Meat")
}
class Giraffe(override val name: String, override val height: Double, override val weight: Double): Animal(name, height, weight){
    override val foodPreferences = arrayOf("Leafs", "Grass")
}
class Elephant(override val name: String, override val height: Double, override val weight: Double): Animal(name, height, weight){
    override val foodPreferences = arrayOf("Grass", "Rye", "Banana", "Apple")
}
class Chimpanzee(override val name: String, override val height: Double, override val weight: Double): Animal(name, height, weight){
    override val foodPreferences = arrayOf("Banana", "Fruits", "Rat")
}
class Gorilla(override val name: String, override val height: Double, override val weight: Double): Animal(name, height, weight){
    override val foodPreferences = arrayOf("Bamboo", "Nuts", "Celery")
}
fun feeding(animals: Array<Animal>, meals: Array<String>){
    for (animal in animals) {
        print("${animal.name} had saturation = ${animal.saturation}, then he ate\t\t")
        for (meal in meals) {
            animal.eat(meal)
            print("$meal and ")
        }
        println("now he have saturation = ${animal.saturation}")
    }
}
package homework4

fun main() {
    val treasureChest = arrayOf(1, 25, 10, 6, 22, 17, 8, 14, 23)
    var captainJoeCoins : Short = 0
    var gangCoins : Short = 0
    for (element in treasureChest) {
        if (element % 2 == 0) {
            captainJoeCoins++
        } else {
            gangCoins++
        }
    }
    print("Captain Joe deserves $captainJoeCoins coins\nand his gang - $gangCoins coins")
}

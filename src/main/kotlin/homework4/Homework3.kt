package homework4

fun main() {
    val myArray = arrayOf(1, -1, -2, 4, 7, 10, 0, 19, -27)
    var isSwapped : Boolean
    for(i in 0 until myArray.size - 1) {
        isSwapped = false
        for(j in 0 until myArray.size - 1 - i) {
            if(myArray[j] > myArray[j+1]) {
                myArray[j] += myArray[j+1]
                myArray[j+1] = myArray[j] - myArray[j+1]
                myArray[j] -= myArray[j+1]
                isSwapped = true
            }
        }
        if (!isSwapped) break
    }

    for (item in myArray) {
        println(item)
    }
}

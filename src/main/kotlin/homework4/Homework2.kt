package homework4

fun main() {
    val marks = arrayOf(3, 4, 5, 2, 3, 5, 5, 2, 4, 5, 2, 4, 5, 3, 4, 3, 3, 4, 4, 5)
    var twos : Short = 0
    var threes : Short = 0
    var fours : Short = 0
    var fives : Short = 0
    for (element in marks){
        when (element){
            2 -> twos++
            3 -> threes++
            4 -> fours++
            5 -> fives++
        }
    }
    print("Отличников - ${fives.toDouble()/marks.size.toDouble()*100}%,\nхорошистов - ${fours.toDouble()/marks.size.toDouble()*100}%,\nтроечников - ${threes.toDouble()/marks.size.toDouble()*100}%,\nдвоечников - ${twos.toDouble()/marks.size.toDouble()*100}%")
}
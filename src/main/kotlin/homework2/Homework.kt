fun main() {
    val lemonade : Int = 18500
    val pinaColada : Short = 200
    val whiskey : Byte = 50
    val dropsOfFresh : Long = 3000000000
    val cocaCola : Float = 0.5F
    val ale : Double = 0.666666667
    val createByBartender : String = "\"Что-то авторское!\""
    println("Заказ - ‘$lemonade мл лимонада’ готов!")
    println("Заказ - ‘$pinaColada мл пина колады’ готов!")
    println("Заказ - ‘$whiskey мл виски’ готов!")
    println("Заказ - ‘$dropsOfFresh капель фреша’ готов!")
    println("Заказ - ‘$cocaCola литра колы’ готов!")
    println("Заказ - ‘$ale литра эля’ готов!")
    print("Заказ - ‘$createByBartender’ готов!")
}